#!/usr/bin/env ruby

require 'optparse'
require 'pathname'
require_relative 'lib/ruby-nicovideo-dl.rb'
require_relative 'lib/queuedb.rb'

# 時間付きputs. log用.
def dputs(arg)
  puts Time.now.strftime("%y/%m/%d %H:%M:%S ") + arg
end

# ------------------------------------
class QueueDownloader
  def initialize(dbfile, savedir)
    @qdb = QueueDB.new(dbfile)
    @svdir = Pathname(savedir)
    @downloader = NicovideoDL::Downloader.new
    @wait = 10 # 間に開ける時間 [s]
  end

  def download_all()
    dl_items = @qdb.get_download_queues
    dputs "QueueDB has #{dl_items.size} items"
    dl_items.each{|id, savefile|
      puts "-"*20 # DL単位区切り棒
      download_movie(id, savefile)
      dputs "wait #{@wait} s"
      sleep(@wait)
    }
    dputs "QueueDB has downloaded"
  end

  def download_movie(id, savefile)
    # 保存先確保
    svf = @svdir + savefile
    svf.dirname.mkpath
    # URL生成
    u = "http://www.nicovideo.jp/watch/#{id}"
    # タイトルを表示しとく
    dputs svf.basename.to_s

    # download(url,economy_skip,savefile)
    begin
      @downloader.download(u, true, svf)
      # 成功時処理
      dputs "success"
      @qdb.update(id, QDB_STATUSCODE[:complete])
      # dputs "QueueDB updated"
    rescue NicovideoDL::NDL_DLError => excp
      dputs "Download error : #{excp[:message]}"
      dputs "info : #{excp[:info]}"
      if :wait != excp[:type]
        @qdb.update(id, QDB_STATUSCODE[excp[:type]])
        # dputs "QueueDB updated"
      end
    end
  end
end

# ------------------------------------
if $0 == __FILE__
  d, s = nil, nil
  opt = OptionParser.new
  opt.on('-d db_file', '--database'){|v| d = v }
  opt.on('-s save_directory', '--save'){|v| s = v }
  opt.parse(ARGV)

  unless d && s
    puts opt.help
    return
  end
  
  dputs "initialize..."
  ml = QueueDownloader.new(d, s)
  dputs "download start"
  ml.download_all()
  dputs "complete"
end

