#!/bin/bash

PJDIR=`dirname $0`
VARDIR=/data/var/nccw
DLDIR=/hdd/download/nicovideo

LOGDIR=/data/var/log/
LOGFILE=$LOGDIR/$(date +%y%m).nccw.log

# queue_downloaderのLogは進捗状況を圧縮するため一時的に別のファイルに保存する
TMPLOG=/hdd/tmp/ncnv/tmp.log

# QueueDB
QDB=$VARDIR/queue.sqlite3
# そのバックアップ場所
QDB_BUDIR=/hdd/tmp/ncnv/


function crawllist(){
  mkdir -p $LOGDIR $VARDIR
  ruby $PJDIR/add_queue.rb -d $QDB -l $PJDIR/mylist.yml | tee -a $LOGFILE
}

function dlqueue(){
  # ディレクトリ生成
  mkdir -p $LOGDIR `dirname $TMPLOG` $QDB_BUDIR
  # QDBバックアップ
  # cp $QDB $QDB_BUDIR/queue.`$LOGD_FMT`.sqlite3
  # 実行
  ruby $PJDIR/queue_downloader.rb -d $QDB -s $DLDIR | tee $TMPLOG
  # log圧縮
  ruby $PJDIR/log_compressor.rb $TMPLOG >> $LOGFILE
}

function usage_exit(){
  echo "Usage: $0 [-l] [-d]" 1>&2
  exit 1
}

if [ $# -lt 1 ]
then
  usage_exit
fi

OPT_L=0
OPT_D=0
while getopts ldh OPT
do
  case $OPT in
    l)	OPT_L=1
        ;;
    d)	OPT_D=1
        ;;
    h)	usage_exit
        ;;
    \?)	usage_exit
        ;;
  esac
done


if [ $OPT_L -eq 1 ]
then
  crawllist
fi

if [ $OPT_D -eq 1 ]
then
  dlqueue
fi
