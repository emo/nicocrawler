#!/usr/bin/env ruby

# DL Queueの管理
# 状態も記録する
# 完了したURLも再DLを防ぐために記録しておく

require 'sqlite3'

QDB_SCHEMA = 'CREATE TABLE IF NOT EXISTS q (
    id TEXT PRIMARY KEY,
    status INTEGER,
    savefile TEXT
)'

QDB_STATUSCODE = {
  :wait => 0, # 登録したて
  :complete => 1, # ダウンロード完了
  :error => 2, # アクセスエラーによりダウンロード失敗
  :not_download => 3, # ダウンロードキャンセル. 2と同じ扱い.
}

class QueueDB < SQLite3::Database
  def initialize(db)
    super(db)
    execute(QDB_SCHEMA)
  end

  # sm893893等のIDと保存先を登録する.
  # 登録成功時にtrueを返す.
  # ダウンロード済み含め既に登録されている場合はfalseを返す.
  # String => true | false
  def add(id, savefile)
    data = [id, QDB_STATUSCODE[:wait], savefile]
    begin
      execute("INSERT INTO q VALUES(?,?,?)", *data)
    rescue SQLite3::ConstraintException => exception
      return false if exception.to_s == "column id is not unique"
      raise exception
    end
    return true
  end

  # DL対象を取得する
  # => [[id, savefile]]
  def get_download_queues
    execute("SELECT id, savefile FROM q WHERE status = ?", QDB_STATUSCODE[:wait])
  end

  # DLしたURLの状態を更新する.
  # statusはSTATUSCODEの元になるsymbol
  def update id, status
    execute("UPDATE q SET status = ? WHERE id = ?", status, id)
  end

end

