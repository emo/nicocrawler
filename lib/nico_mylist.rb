#!ruby -Ku
# ニコニコ動画のマイリストを読み込む

require 'pathname'
require 'rss'
require 'net/http'
# require 'nokogiri'

# :title,:link,:date,:description に格納
class NicoMyListItem < Hash
  def initialize item
    %w!title link date description!.each{|s|
      store(s.to_sym, item.__send__(s))
    }
  end
end

# [{:title,:link,:date,:description}] で格納
class NicoMyList < Array
  def add_mylist mylist_id
    uri = URI("http://www.nicovideo.jp/mylist/#{mylist_id}?rss=2.0&lang=ja-jp")
    res = Net::HTTP.get(uri)
    rss = RSS::Parser.parse(res)
    rss.items.each{|item|
      self << NicoMyListItem.new(item)
    }
  end
end

