#!/usr/bin/env ruby

#----
# ruby-nicovideo-dl.rb
#----
# [Set up]
# 1. Install Netrc gem
#    Use gem command.
#
#    $ gem install netrc
#
# 2. Put the '.netrc' file
#    Put the following file '.netrc' in your home directory.
#   
#    ==> ${HOME}/.netrc <==
#    machine nicovideo
#    login (e-mail address for nicovideo)
#    password (password for nicovideo)
#
# 3. Install RTMPDump
#    Install RTMPDump from "https://rtmpdump.mplayerhq.hu/".
#
# [Usage]
# $ ./ruby-nicovideo-dl.rb (video URL(s) of nicovideo)
#----

require_relative 'nvdl-librtmp.rb'
require_relative 'nvdl-exception'
require 'netrc'
require 'net/http'
require 'net/https'
require 'cgi'
require 'ffi'

module NicovideoDL
  Version = "0.2.0" + " MOD"

  def exit_with_error(message)
    # $stderr.puts "Error: #{message}"
    puts "Error: #{message}"
    exit(1)
  end
  module_function :exit_with_error

  class DownloadStatusPrinter
    Size1K = 1024
    Epsilon = 0.0001

    def initialize(total_size=0, unit=:byte)
      @start_time = Time.now
      @total_size = total_size
      @unit = unit
    end

    attr_accessor :total_size

    def print(current_size)
      if @total_size != 0
        percent = (100.0 * current_size) / @total_size
        percent_str = sprintf("%.1f", percent)
        eta_str = get_eta_str(current_size)
      else
        percent_str = "---.-"
        eta_str = "--:--"
      end

      speed_str = get_speed_str(current_size)
      current_size_str = format(current_size)
      total_size_str = format(@total_size)
      $stdout.printf("\rDownloading video data: %5s%% (%8s of %s) at %12s ETA %s ",
                     percent_str, current_size_str, total_size_str, speed_str, eta_str)
      $stdout.flush
    end

    private

    def get_eta_str(current_size)
      speed = calculate_speed(current_size)
      if speed
        rest_size = @total_size - current_size
        eta = (rest_size / speed).to_i
        eta_min, eta_sec = eta.divmod(60)
        if eta_min > 99
          "--:--"
        else
          sprintf "%02d:%02d", eta_min, eta_sec
        end
      else
        "--:--"
      end
    end

    def get_speed_str(current_size)
      speed = calculate_speed(current_size)
      "#{format(speed)}/sec"
    end

    def calculate_speed(current_size)
      elapsed = Time.now - @start_time
      if (current_size == 0) || (elapsed < Epsilon)
        nil
      else
        current_size.to_f / elapsed
      end
    end

    def format(value)
      if @unit == :byte
        format_byte(value)
      elsif @unit == :sec
        format_sec(value)
      else
        "#{value}"
      end
    end

    def format_byte(value)
      if value
        exp = (value > 0) ? Math.log(value, Size1K).to_i : 0
        suffix = "bKMGTPEZY"[exp]
        if exp == 0
          "#{value}#{suffix}"
        else
          converted = value.to_f / (Size1K ** exp)
          sprintf "%.2f%s", converted, suffix
        end
      else
        "N/A b"
      end
    end

    def format_sec(value)
      if value
        if value > 60
          converted = value.to_f / 60
          sprintf "%.1fmin", converted
        else
          sprintf "%.1fsec", value.to_f
        end
      else
        "N/A sec"
      end
    end
  end

  class Downloader
    LoginURI = URI.parse("https://secure.nicovideo.jp/secure/login?site=niconico")
    LoginPostFormat = "current_form=login&mail=%s&password=%s&login_submit=Log+In"

    VideoHost = "www.nicovideo.jp"
    VideoPathFormat = "/watch/%s"
    VideoURLRegexp = %r{^(?:(?:http://)?(?:\w+\.)?(?:nicovideo\.jp/(?:v/|(?:watch(?:\.php)?))?/)?(\w+))}

    VideoInfoPathFormat = "/api/getflv?v=%s&as3=1"
    VideoTypeRegexp = %r{^http://.*\.nicovideo\.jp/smile\?(.*?)=.*}

    def initialize
      login
    end

    # url, economy_skip: 文字通り
    # savefile: 保存先. 拡張子は後付される.
    # Economyのskip時含め問題発生時は NDL_DLErrorが投げられる.
    # 予期しない問題はexitが呼ばれるため注意.
    def download(url, economy_skip = false, savefile = nil)
      puts "Download #{url}"

      video_id = get_video_id(url)
      video_cookies = get_video_cookies(video_id)
      video_info = get_video_info(video_id, video_cookies)
      return video_info if Symbol === video_info # 配信停止/削除を検出
      video_extension = get_video_extension(video_info["uri"])
      output_filename = "#{savefile || video_id}#{video_extension}"

      puts "- video  URL : #{video_info['uri'].to_s}"
      puts "- output file: #{output_filename}"
      
      # エコノミー確認
      if economy_skip && video_info["uri"].query =~ /low$/
        raise NDL_DLError.new(type: :wait, info: video_info, message: "Economy mode")
      end

      if video_info["uri"].scheme == "http"
        download_with_http(video_info, video_cookies, output_filename)
      elsif video_info["uri"].scheme == "rtmpe"
        download_with_rtmpe(video_info, video_cookies, output_filename)
      else
        NicovideoDL.exit_with_error("Unsupported scheme. [scheme=#{video_info['uri'].scheme}]")
        return :error
      end

      puts "done."
      return nil
    end

    private

    def login
      unless @user_session
        $stdout.print("Login...")
        $stdout.flush

        netrc_info = Netrc.read
        user, password = netrc_info["nicovideo"]
        if user.nil? || user.empty? || password.nil? || password.empty?
          NicovideoDL.exit_with_error("Netrc is invalid.")
        end

        https = Net::HTTP.new(LoginURI.host, LoginURI.port)
        https.use_ssl = true
        https.verify_mode = OpenSSL::SSL::VERIFY_NONE
        postdata = sprintf(LoginPostFormat, user, password)

        response = https.post(LoginURI.request_uri, postdata)
        response.get_fields('set-cookie').each do |cookie|
          key, value = cookie.split(';').first.split('=')
          if (key == 'user_session') && (value != 'deleted')
            @user_session = value
            break
          end
        end
        if @user_session.nil?
          NicovideoDL.exit_with_error("Failed to login.")
        end

        puts "done."
      end
    end

    def get_video_id(url)
      if match_data = VideoURLRegexp.match(url)
        match_data[1]
      else
        NicovideoDL.exit_with_error("URL is invalid. [url=#{url}]")
      end
    end

    def get_video_cookies(video_id)
      video_cookies = Hash.new
      video_cookies['user_session'] = @user_session

      http = Net::HTTP.new(VideoHost, 443)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      video_path = sprintf(VideoPathFormat, video_id)

      response = http.get(video_path, make_http_header(video_cookies))
      response.get_fields('set-cookie').each do |cookie|
        key, value = cookie.split(';').first.split('=')
        if key == 'nicohistory'
          video_cookies[key] = value
          break
        end
      end
      video_cookies
    end

    def get_video_info(video_id, video_cookies)
      video_path = sprintf(VideoPathFormat, video_id)
      original_uri = URI::HTTP.build(host: VideoHost, path: video_path)

      http = Net::HTTP.new(VideoHost)
      video_info_path = sprintf(VideoInfoPathFormat, video_id)
      response = http.get(video_info_path, make_http_header(video_cookies))

      while response.is_a?(Net::HTTPRedirection)
        redirect_uri = URI.parse(response.get_fields('location').first)
        http = Net::HTTP.new(redirect_uri.host)
        response = http.get(redirect_uri.request_uri, make_http_header(video_cookies))
      end

      begin
        info = CGI.parse(response.body)

        # 配信停止/削除済みの確認
        NDL_DLError.check_info(info)

        video_url = info['url'].first
        video_uri = URI.parse(video_url)

        if video_uri.scheme == "http"
          {"uri" => video_uri}
        else
          fmst2, fmst1 = info['fmst'].first.split(':')
          {"uri" => video_uri, "original_uri" => original_uri,
            "fmst1" => fmst1, "fmst2" => fmst2}
        end
        
      rescue NDL_DLError => dle
        raise dle
      rescue
        NicovideoDL.exit_with_error("Failed to access video information.")
      end
    end

    def get_video_extension(video_uri)
      if match_data = VideoTypeRegexp.match(video_uri.to_s)
        if match_data[1] == "s"
          ".swf"
        elsif match_data[1] == "m"
          ".mp4"
        else
          ".flv"
        end
      else
        ".flv"
      end
    end

    def download_with_http(video_info, video_cookies, output_filename)
      total_size, current_size = 0, 0 # サイズ確認のためscopeを外に出す

      http = Net::HTTP.new(video_info["uri"].host)
      http.request_get(video_info["uri"].request_uri, make_http_header(video_cookies)) do |response|
        total_size = response.get_fields('Content-length').first.to_i rescue 0
        File.open(output_filename, "wb") do |file|
          download_status_printer = DownloadStatusPrinter.new(total_size, :byte)
          response.read_body do |video_block|
            download_status_printer.print(current_size)
            current_size += video_block.bytesize
            file.write(video_block)
          end
          download_status_printer.print(current_size)
        end
      end

      # サイズ確認 (DL失敗問題対処) *File.open(){~}の外で無いとremoveに問題あり
      if (current_size != total_size) || (total_size < (1<<20)) then
        puts("") # logの改行調整
        File.delete(output_filename)
        NDL_DLError.throw_download_size_error()
      end

    end

    def download_with_rtmpe(video_info, video_cookies, output_filename)
      original_uri = video_info["original_uri"]
      uri = video_info["uri"]
      playpath = uri.query.split('=')[1]
      fmst1 = video_info["fmst1"]
      fmst2 = video_info["fmst2"]

      status = LibRTMP::RD_SUCCESS
      resume_info = nil
      download_status_printer = DownloadStatusPrinter.new(0, :sec)

      LibRTMP::FLVFile.open(output_filename, "w+b") do |file|
        loop do
          if resume_info && resume_info.fully_downloaded?
            status = LibRTMP::RD_SUCCESS
            break
          end

          rtmp = LibRTMP::RTMP.create_from_uri(uri, original_uri)
          rtmp.add_connect_option("S:#{video_info['fmst1']}")
          rtmp.add_connect_option("S:#{video_info['fmst2']}")
          rtmp.add_connect_option("S:#{playpath}")

          rtmp.open(resume_info) do 
            status = rtmp.read_data do |data, current_sec, total_sec|
              file.write(data)

              if (download_status_printer.total_size == 0) && (total_sec > 0)
                download_status_printer.total_size = total_sec
              end
              download_status_printer.print(current_sec)
            end
          end

          if status == LibRTMP::RD_INCOMPLETE
            resume_info = file.get_resume_info
            file.pos = resume_info.last_file_position
          else
            break
          end
        end
      end
    end

    def make_http_header(cookies)
      cookie_str = cookies.map do |key, value|
        "#{key}=#{value};"
      end.join(' ')
      {'Cookie' => cookie_str}
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  if ARGV.size < 1
    raise "URL is not specified."
  end

  downloader = NicovideoDL::Downloader.new
  ARGV.each do |url|
    downloader.download(url)
  end
end
