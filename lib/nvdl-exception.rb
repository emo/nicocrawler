#!/bin/ruby

# ruby-nicovideo-dlの例外を記載. 基本的に外で補足.

module NicovideoDL

  class NDL_Exception < Exception; end

  # Download中の失敗
  class NDL_DLError < NDL_Exception
    # 引数はHashで渡す
    # :type: QDB_STATUSCODEのkey. :wait, :complete, :errorのどれか.
    # :info: video_info等
    # :message: 表示するメッセージ
    def initialize(arg)
      @data = arg
    end
    
    def [](key); @data[key]; end
    attr_reader :data

    # Downloadサイズ不一致エラーを投げる (DL失敗問題対処)
    def self.throw_download_size_error()
      # 再ダウンロードのためにtypeはwaitにしておく
      raise NDL_DLError.new(type: :wait, info: nil, message: "Download size error")
    end

    # 動画情報を確認し,配信停止/削除済みを検出.
    # 対象の場合は例外を投げる.
    # get_video_infoで使用.
    def self.check_info(info)
      puts "info: #{info}"
      
      if info.key?('error')
        case erv = info['error'].first
        when 'invalid_v1'
          raise NDL_DLError.new(type: :error, info: info, message: "Access denied")
        when 'Access_locked'
          raise NDL_DLError.new(type: :wait, info: info, message: erk)
        else
          raise NDL_DLError.new(type: :wait, info: info, message: "Unknown error: #{erk}")
        end
      elsif info.key?('deleted')
        raise NDL_DLError.new(type: :error, info: info, message: "This video has been deleted.")
      elsif info['url'].first.empty?
        raise NDL_DLError.new(type: :error, info: info, message: "No video URL")
      end
    end


  end

end

