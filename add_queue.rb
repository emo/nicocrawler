#!/usr/bin/env ruby
# 
# mylistにある新規動画をDBに登録していく
# 設定ファイルは全部コマンドラインで渡す

require 'optparse'
require 'yaml'
require 'pathname'
require_relative 'lib/queuedb.rb'
require_relative 'lib/nico_mylist.rb'


# 時間付きputs. log用.
def dputs(arg)
  puts Time.now.strftime("%y/%m/%d %H:%M:%S ") + arg
end

# ファイル名に使えない文字列を置換
def rep_filename(file)
  file.tr('\\\\/:*?"<>#|―','＼_：＊？”＜＞＃｜-')
end

# -----------------------------------
class Mylist
  def initialize(db, listfile)
    @qdb = QueueDB.new(db)
    @list = YAML.load_file(listfile)
  end

  def crawlall()
    @list.each{|item| crawllist(item) }
  end

  def crawllist(item)
    dest = Pathname(item['dest'] || "")
    filter = (item['filter'] || []).collect{|v| Regexp.new(v) }
    dfilter = item['dest_filter'] || []

    dputs "crawl mylist : #{item['id']}"
    nml = NicoMyList.new
		nml.add_mylist(item['id'])
    dputs "#{nml.size} items"

    cnt_add, cnt_skip = 0, 0
    nml.each{ |nico_item|
      t = nico_item[:title]
      m = nil
      mvid = Pathname(nico_item[:link]).basename.to_s

      # filter適用
      if m = filter.find{|f| t =~ f }
        dputs "skip at #{m} : [#{mvid}] #{t}"
        cnt_skip += 1
        next
      end

      # dfilterで送り先決定. 無いならそのまま.
      d = dest + (dfilter.find{|f| t.index(f) } || "")
      # ファイル名決定
      fn = rep_filename(t) + " - [#{mvid}]"

      # queue追加
      if @qdb.add(mvid, (d + fn).to_s)
        # 追加成功時
        cnt_add += 1
        dputs "add : #{t} [#{mvid}] => #{d}"
      else
        # 追加済みなら次.
      end
    }
    dputs "added #{cnt_add}, skiped #{cnt_skip}"
  end

end

# ------------------------------------
if $0 == __FILE__
  db, lfile = nil, nil
  opt = OptionParser.new
  opt.on('-d db_file', '--database'){|v| db = v }
  opt.on('-l listfile', '--listfile'){|v| lfile = v }
  opt.parse(ARGV)

  unless db && lfile
    puts opt.help
    return
  end

  dputs "initialize..."
  ml = Mylist.new(db, lfile)
  dputs "crawl start"
  ml.crawlall()
  dputs "complete"
end
