#!/usr/bin/env ruby

# 抽出中間行の数
MIDDLE_NUM = 10

def compress_download(l)
  # 先頭に\rが来るためd[0]は""
  d = l.split("\r")
  r = []
  # 要素回収
  idx, imax = Rational(1), Rational(d.size() -1)
  di = (imax -idx) / MIDDLE_NUM
  while idx < imax
    r << d[idx.to_i]
    idx += di
  end
  r << d.last

  return r.join("\n")
end

while l = ARGF.gets
  case l
  when /^\r/
    puts compress_download(l)
  else
    puts l
  end
end
